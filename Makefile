PREFIX ?= /usr/local

CC ?= gcc
CFLAGS += -Wall -Werror -std=c99 -D_GNU_SOURCE
OBJS += main.o sbasm.o util.o

all: sbasm
sbasm: $(OBJS)
	$(CC) $(OBJS) -o sbasm

%.o: src/%.c
	$(CC) -c $(CFLAGS) $< -o $@

install:
	cp sbasm $(PREFIX)/bin
