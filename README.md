# SBASM

SBASM stands for Stack-Based Assembly like language.

Simple stack-based language.

Just a fun project, not intended for production. It is not even turing complete (I think), you are stuck using a pre-defined set of keywords.

The syntax reminds me of Assembly languages so that's why its called SBASM.

## Keywords

So far the keywords are:

- pushStr:
    
    Accepts 1 argument which is a string. Pushes the string to the stack.
    
    `pushStr "hello"`
- pushNum:

    Acccepts 1 argument which is a number. Pushes the number to the stack.
    
    `pushNum 4`
- dbg:
    
    Accepts 1 argument which is a string and prints it to stderr.
    
    There is also dbg2 which is the same but prints to stdout :)

    Useful if you dont want to use pushStr and print to print a hardcoded value.
    
    `dbg "goodbye"`
- print:
    
    Prints the latest item added to the stack to stdout.
    
    `print`
- error:
    
    Prints the latest item added to the stack to stderr.
    
    `error`
- newline:
    
    Prints a newline to stdout.
    
    `newline`
- add:
    
    Adds the 2 latest items added to the stack.
    
    `add`
- sub:
    
    Subtracts the 2 latest items added to the stack.
    
    `sub`
- mul:
    
    Multiplies the 2 latest items added to the stack.
    
    `mul`
- div:
    
    Divides the 2 latest items added to the stack.
    
    `div`
- scan:
    
    Reads a number from STDIN
    
    `scan`
- read:
    
    Reads a string line from STDIN
    
    `read`
- rng:
    
    Returns random number with max value of X
    
    `rng 10`
- drop:

    Forgets about the last item on the stack, otherwise meaning destroying it.
    
    `drop`
- jmp:
    
    Jmp statement brings you to label X
    
    `jmp 1`
- label:
    
    Stores a label named X (X can only be an integer (Max size is 4096))

    `label 4`

- jg:

    Jumps to line X unless item on top of the stack is 1
    
    `jg 37`
