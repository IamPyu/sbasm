# Counter Program

# start at 0
pushNum 0

# store a label to line 8
label 1

# add 1
pushNum 1
add

# duplicate so we dont lose track
dup

# print
print
newline

# sleep and go back to line 8 using our label
sleep 1
jmp 1
