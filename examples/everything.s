# Often updated examples

; You can also use semicolons for comments!

## Hello World!
dbg "Hello World!"
newline

### You can also do hello world like this:

pushStr "Hello World!"
print
newline

### And:

pushStr "Hello World!"
error
newline

## Maths

### this is a equation using every arithmetic operation in sbasm.
### it results in 0.5

dbg "5 + 3 - 7 * 2 / 4: "
pushNum 5
pushNum 3
add
pushNum 7
sub
pushNum 2
mul
pushNum 4
div
print
newline

## Dup

### Duplicates '5' and adds them resulting in 10.

dbg "5 + 5: "
pushNum 5
dup
add
print
newline

## Random Values

dbg "I am the magic pseudo-random generator: "
rng 10
print
newline

## Input

### Read a number
dbg "Enter a number and I will add 5 to it: "
scan
pushNum 5
add
dbg "Your answer is: "
print
newline

### Read a string

dbg "Enter anything you like: "
read
dbg "You entered: "
print
newline

# Drop

## Here we have 6 on the top of the stack.
pushNum 6
## Now we have 4.
pushNum 4

## If we drop it, that 4 is lost forever.
drop
print
newline
