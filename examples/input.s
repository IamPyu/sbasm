# Read user input example

# Read number from user
dbg "Enter a number: "
scan
dbg "You entered: "
print
newline

# Read string from user
dbg "Enter anything: "
read
dbg "You entered: "
print
newline
