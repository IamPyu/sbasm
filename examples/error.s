# Errors

# Print "This is an error." to stderr.
pushStr "This is an error."
error
newline

# The dbg keyword also prints to stderr.
dbg "This is also printed to stderr."
newline
