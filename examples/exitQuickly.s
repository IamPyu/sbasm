# In the interpreter, if the text EOF is reached the program stops!
dbg2 "After an EOF phrase is reached, the program exits."
newline
dbg2 "Check the source code of this example to see how!"
newline
EOF

dbg2 "I am never reached :("
newline
