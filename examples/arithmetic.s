# Arithmetic Example
#
# Task:
#
# This Program adds 5 and 10 and then subtracts 5 from the sum and then divides from the difference.
# Resulting in 5


pushNum 5
pushNum 10
add

pushNum 5
sub

pushNum 2
div

print
newline
