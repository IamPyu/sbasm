dbg2 "Number adder program"
newline

label 1

dbg2 "Enter a number: "
scan

pushNum 2
add
print
newline

dbg2 "Try again? y/n: "
read

pushStr "y"

# jump to label 1 if item on stack is not equal to other item on stack
eq 
jg 1
