# See how SBASM parses strings.

pushStr "hello world"
print
newline

# pushing a string that will fail to parse returns 0, otherwise known as null in this case.
pushStr failing string"
print
newline

pushStr "Cool string"
print
newline

# you can also use single quotes for string parsing
pushStr 'You can also parse strings with single quotes!'
print
newline
