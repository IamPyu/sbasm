with import <nixpkgs> {};
stdenv.mkDerivation {
  name = "sbasm";
  src = ./.;
  nativeBuildInputs = with pkgs; [];
  buildInputs = with pkgs; [];
  buildPhase = "make -B";
  installPhase = "mkdir -p $out && make PREFIX=$out install";
}
