#ifndef SBASM_H
#define SBASM_H

#ifndef STACK_SIZE
#define STACK_SIZE 256
#endif

#define strsize 512 * sizeof(char*)
#define maxlines 5000

#include <stdio.h>

// Types

typedef struct {
  union Node {
    char *s; // String
    double f; // Number
    int p; // Placeholder for checking equality, etc.
  } value;

  enum Type {
    T_STRING,
    T_NUMBER
  } type;
} Value;

typedef struct {
  int top;
  Value *arr;
} Stack;

// Values

Value new(enum Type T);
Value null();

void print(Stack *s, FILE *f); // Prints a item to the stack.
void scan(Stack *s); // Use scanf to read a number.
void read_(Stack *s); // Use fgets to read a string.

void add(Stack *s); // Adds last 2 items from the stack.
void sub(Stack *s); // Subtracts last 2 items from the stack.
void mul(Stack *s); // Multiplies last 2 items from the stack.
void div_(Stack *s); // Divides last 2 items from the stack. (It is called div_ so it does not conflict with stdlib.h's div)
void mod(Stack *s); // Gets the remainder of the last 2 items from the stack.

void dup_(Stack *s); // Duplicates last item from the stack.
void rng(Stack *s); // Returns a random where the maxiunum is the latest item added to the stack.

// Relation ship operators

void eq(Stack *s);
void ne(Stack *s);
void gt(Stack *s);
void lt(Stack *s);

// Stack

Stack *newStack();
void freeStack(Stack *stack);
int stackFull(Stack *stack);
int stackEmpty(Stack *stack);

void stackPush(Stack *stack, Value item);
Value *stackPop(Stack *stack);
Value *stackPeek(Stack *stack);

#endif // SBASM_H
