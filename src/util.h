#ifndef UTIL_H
#define UTIL_H


#include "sbasm.h"

Value parseString(char *s, int l);
Value parseNumber(char *s);

#endif
