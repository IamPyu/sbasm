#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "sbasm.h"

// Value

Value new(enum Type T) {
  Value e = {0};
  e.type = T;
  return e;
}

Value null() {
  Value e = new(T_NUMBER);
  e.value.f = 0;
  return e;
}

void print(Stack *s, FILE *f) {
  Value *v = stackPop(s);

  switch (v->type) {
  case T_NUMBER:
    fprintf(f, "%lf", v->value.f); break;
  case T_STRING:
    fprintf(f, "%s", v->value.s); break;
  default:
    printf("Unsupported type."); break;
  }
}

void scan(Stack *s) {
  Value v = new(T_NUMBER);
  float x;

  char str[strsize];
  fgets(str, strsize, stdin);
  x = strtod(str, NULL);

  v.value.f = x;
  stackPush(s, v);
}

void read_(Stack *s) {
  Value v = new(T_STRING);

  char str[strsize];
  fgets(str, sizeof(str), stdin);
  
  str[strlen(str)-1] = '\0';
  
  v.value.s = str;
  
  stackPush(s, v);
}

#define makeOp(o)\
  Value *lth = stackPop(s);\
  Value *rth = stackPop(s);\
  if (rth == NULL || lth == NULL) goto nil;\
  if (lth->type == T_NUMBER && rth->type == T_NUMBER) {\
    Value ans = new(T_NUMBER);\
    ans.value.f = rth->value.f o lth->value.f;\
    stackPush(s, ans);\
  } else {\
    perror("Both lth and rth must be number types.");\
    nil: stackPush(s, null());\
  }

void add(Stack *s) {
  makeOp(+);
}

void sub(Stack *s) {
  makeOp(-);
}

void mul(Stack *s) {
  makeOp(*); 
}

void div_(Stack *s) {
  makeOp(/); 
}

void dup_(Stack *s) {
  Value *v = stackPeek(s);
  stackPush(s, *v);
}

void rng(Stack *s) {
  Value *a = stackPop(s);

  if (a->type != T_NUMBER) {
    stackPush(s, null()); /* if max range for number is not type number, return null */
    return;
  }

  srand(time(NULL));
  double r = (double)(rand() % (int)a->value.f) - 1;

  Value v = new(T_NUMBER);
  v.value.f = r;
  stackPush(s, v);
}

#define makeRelOp(o, z)\
  Value *lth = stackPop(s);\
  Value *rth = stackPop(s);\
  Value x = null();\
  if (rth->type == T_NUMBER && lth->type == T_NUMBER){\
    x.value.f = (rth->value.f o lth->value.f);\
  } else if (rth->type == T_STRING && lth->type == T_STRING){\
    x.value.f = (double)strcmp(rth->value.s, lth->value.s) o z;\
  } else {\
    perror("Both lth and rth must be the same type.");\
    stackPush(s, null()); return;\
  }\
  stackPush(s, x);

void eq(Stack *s) {
  makeRelOp(==, 0);
}

void ne(Stack *s) {
  makeRelOp(!=, 0);
}

void gt(Stack *s) {
  makeRelOp(>, 0);
}

void lt(Stack *s) {
  makeRelOp(<, 0);
}

// Stack
// --- Stack (De)Constructor and Boolean operations ---

Stack *newStack() {
  Stack *stack = malloc(sizeof(Stack));
  stack->top = -1;
  stack->arr = malloc(STACK_SIZE * sizeof(Value));
  return stack;
}

void freeStack(Stack *stack) {
  free(stack);
}

int stackFull(Stack *stack) {
  return stack->top == STACK_SIZE-1;
}

int stackEmpty(Stack *stack) {
  return stack->top == -1;
}

// --- Core Stack operations ---

void stackPush(Stack *stack, Value item) {
  if (stackFull(stack)) return;
  stack->arr[++stack->top] = item;
}

Value *stackPop(Stack *stack) {
  if (stackEmpty(stack)) return NULL;
  return &stack->arr[stack->top--];
}

Value *stackPeek(Stack *stack) {
  if (stackEmpty(stack)) return NULL;
  return &stack->arr[stack->top];
}
