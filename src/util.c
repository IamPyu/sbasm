#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "sbasm.h"
#include "util.h"

Value parseString(char *s, int l) {
  int t1 = s[0] == '"' && s[strlen(s)-1] == '"';
  int t2 = s[0] == '\'' && s[strlen(s)-1] == '\'';

  if (t1 || t2) {
    s++;
    s[strlen(s)-1] = '\0';

    Value str = new(T_STRING);
    str.value.s = s;
    return str;
  }
  fprintf(stderr, "String parse failed at line %d, have you surrounded your string with `'` or `\"`?\n", l);
  return null();
}

Value parseNumber(char *s) {
  double n = atof(s);

  Value num = new(T_NUMBER);
  num.value.f = n;

  return num;
}
