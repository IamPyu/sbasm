#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>

#include "sbasm.h"
#include "util.h"

static Stack *stack = NULL;

void cleanup();
void runProgram(char **lines);

int main(int argc, const char *argv[]) {
  signal(SIGINT, cleanup);
  stack = newStack();

  if (argc > 1) {
    FILE *file = fopen(argv[1], "r");

    if (file == NULL) {
      printf("Error: file '%s' does not exist\n", argv[1]);
      fclose(file);
      cleanup();
    }

    char *lines[maxlines]; 
    memset(&lines, 0, sizeof(lines));

    int i = 0;

    char *cline = NULL;
    size_t len = 0;

    while (getline(&cline, &len, file) != -1) {
      cline[strlen(cline)-1] = '\0';
      if (strcmp(cline, "") == 0) { /* empty lines break the entire interpreter for some reason, so we skip them */
        lines[i] = NULL;
        goto add;
      }

      lines[i] = strdup(cline);
add: i++;
    }
    lines[i] = "EOF"; 

    runProgram(lines);
  } else {
    printf("Usage: %s [FILE]\n", argv[0]);
  }

  cleanup();
  return 0;
}


void cleanup() {
  if (stack != NULL) {
    freeStack(stack);
  } 

  printf("\n\nSBASM Process (%d) Finished\n", getpid());
  exit(0);
}

int streq(const char *s1, const char *s2) {
  return strcmp(s1, s2) == 0;
}

void runProgram(char **lines) {
  int labels[4096] = {};

  for (int i = 0; i < 5000; i++) {
    if (lines[i] == NULL) continue;
    if (streq(lines[i], "EOF")) break;

    char *line = lines[i];
    int lineNum = i + 1;

    char cmd[strsize];
    char args[strsize];

    int nargs = sscanf(line, "%s %[^\t]", cmd, args);
    
    if (cmd[0] == '#' || cmd[0] == ';') continue; /* comments */

    if (streq(cmd, "pushStr") && nargs == 2) {
      stackPush(stack, parseString(args, lineNum)); 
    } else if (streq(cmd, "pushNum") && nargs == 2) {
      stackPush(stack, parseNumber(args));
    } else if (streq(cmd, "dbg")) {
      Value x = parseString(args, lineNum);
      stackPush(stack, x);
      print(stack, stderr);
    } else if (streq(cmd, "dbg2")) {
      Value x = parseString(args, lineNum);
      stackPush(stack, x);
      print(stack, stdout);
    } else if (streq(cmd, "print")) {
      print(stack, stdout);
    } else if (streq(cmd, "error")) {
      print(stack, stderr);
    } else if (streq(cmd, "newline")) {
      printf("\n");
    } else if (streq(cmd, "add")) {
      add(stack);
    } else if (streq(cmd, "sub")) {
      sub(stack);
    } else if (streq(cmd, "mul")) {
      mul(stack);
    } else if (streq(cmd, "div")) {
      div_(stack);
    } else if (streq(cmd, "scan")) {
      scan(stack);
    } else if (streq(cmd, "read")) {
      read_(stack);
    } else if (streq(cmd, "rng")) {
      Value n = parseNumber(args);
      stackPush(stack, n);
      rng(stack);
    } else if (streq(cmd, "dup")) {
      dup_(stack);
    } else if (streq(cmd, "drop")) {
      stackPop(stack); 
    } else if (streq(cmd, "jmp")) {
      i = labels[((int)parseNumber(args).value.f)];
    } else if (streq(cmd, "sleep")) {
      Value ms = parseNumber(args);
      sleep((int)ms.value.f);
    } else if (streq(cmd, "usleep")) {
      Value ms = parseNumber(args);
      usleep((int)ms.value.f);
    } else if (streq(cmd, "jg")) {
      Value *cnd = stackPop(stack);
      Value ln = parseNumber(args);

      if ((int)cnd->value.f != 0) {
        i = labels[((int)ln.value.f)];
      }
    } else if (streq(cmd, "eq")) {
      eq(stack);
    } else if (streq(cmd, "ne")) {
      ne(stack);
    } else if (streq(cmd, "gt")) {
      gt(stack);
    } else if (streq(cmd, "lt")) {
      lt(stack);
    } else if (streq(cmd, "peek")) {
      Value *v = stackPeek(stack);

      if (v->type == T_NUMBER) {
        printf("%lf\n", v->value.f);
      } else {
        printf("%s\n", v->value.s);
      }
    } else if (streq(cmd, "label")) {
      int l = ((int)parseNumber(args).value.f);
      labels[l] = i - 1;
    } else {
      fprintf(stderr, "Invalid keyword '%s' at line: %d\n", cmd, lineNum);
    }
  }
}
